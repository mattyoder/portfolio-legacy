#!/bin/sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

convert $1 \
  -background None \
  -resize 400x550^ \
  -gravity northwest \
  -extent 400x550 \
  -virtual-pixel Transparent \
  -set option:distort:viewport 450x550-0-0 \
  -distort Shepards "0,0 0,0  400,0 400,0  0,50 0,50  400,50 400,50  0,100 0,100  400,100 400,100  0,150 0,150  400,150 400,150  0,200 0,200  400,200 400,200  0,250 0,250  400,250 400,250  0,800  150,800  400,800  550,800" +repage \
  -page -56+0 $DIR/shadow.png +swap \
  -page -25-20 $DIR/tape.png \
  -background none \
  -layers merge \
  -set option:distort:viewport 600x700-50-50 \
  -rotate -5 +repage \
  -sharpen 1 \
  -flatten \
  -trim `dirname $1`/poster.png

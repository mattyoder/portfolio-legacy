---
title: Flaxton Park Master Plan
date: 2012-12-16T12:00:00-06:00
draft: false
greeting: Impact.
project_url: http://static.mattyoder.com/mattyoder/files/flaxton-park.pdf
description: >
  In this class project, I created a master plan for Flaxton Park, a fictitious
  810-acre development near Mahomet, Illinois.
---

# The problem.
Champaign County's population is projected to increase 37.7% between 2010 and
2035. Development to accommodate this population growth could disrupt the
county's natural systems, but it doesn't have to do so. In this class project, I
created a master plan for a fictitious development to demonstrate techniques for
developing both sustainably and profitably.

## The solution.
Flaxton Park is a sustainable, mixed-use development proposed for an 810-acre
site southeast of Mahomet, Illinois. The Flaxton Park community is designed
around the idea of low-impact development, a paradigm that minimizes the
hydrologic impact of development by reducing impervious surfaces and applying
other best management practices. In its land use, transportation, stormwater,
sewer and green infrastructure, the plan takes advantage of—and minimizes the
disruption to—natural systems.

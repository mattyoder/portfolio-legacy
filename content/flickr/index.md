---
title: Flickr and the Postmodern Post-Photo
date: 2008-12-01T12:00:00-06:00
draft: false
greeting: Click.
project_url: http://flickr.mattyoder.com/
description: >
  In this Web essay, I explore the relationship between the photo-sharing site
  Flickr and post-photography.
---

# The shift.
The digital age has ushered in a new age of photography, but the shift has more
to do with behaviors and attitudes than technology. Photography directly
corresponds to reality; post-photography doesn't. Photography captures to
remember; post-photography captures to define. Photographers tell stories about
photos; post-photographers tell stories with photos. Together these changes
indicate a marked shift in the mindset of the people behind the camera.

## The story.
Few factors have been more influential in the shift to post-photography than the
popular photo-sharing site [Flickr](http://flickr.com/). In [this
essay](http://flickr.mattyoder.com/), I explore the ways that Flickr has both
shaped and been shaped by post-photography.

---
title: Expository Writing Portfolio
date: 2006-11-01T12:00:00-06:00
draft: false
greeting: Expos.
project_url: http://expos.mattyoder.com/
description: >
  Technology and writing have always been two of my great loves. In three
  essays, I bring them together with results that range from weighty to
  lighthearted.
---

# The subject.
Technology and writing have always been two of my great loves. In three essays,
I bring them together with results that range from weighty to lighthearted.
Regardless of the gravity, my writing explores the ways that we shape and are
shaped by all things digital.

## The challenge.
Though technology is one of my passions, I discovered early in the writing
process that not everyone shares my interest. I found it difficult to relate to
an audience that is not inherently fascinated by the technical underpinnings of
the Internet or the fine points of operating system design. I struggled to
strain from my vocabulary the technical jargon that has become an integral part
of the way I speak and think. I labored to explain concepts that are second
nature to those who have grown up—as I have—surrounded by technology. In the
process, I gained a greater appreciation for readers, both high- and low-tech,
and for the brave writers who, through user's manuals and tutorials, attempt to
bridge the technical divide.

## The essays.
In these essays, I explore the world of technology through the lenses of the
scholar, the critic and the lighthearted philosopher.

### ["Pipe" Dreams: The Future of a Non-Neutral Internet](http://expos.mattyoder.com/doc1.html)
The idea behind this essay came from an e-mail I received from eBay CEO Meg
Whitman. In her message, Whitman warned that Internet service providers were
trying to create a "two-tiered system" that would effectively divide the Web
into fast and slow lanes. Alarmed, I did some preliminary research that
uncovered the far-reaching consequences of this technologically and socially
important issue. As an aspiring Internet professional, I sense the urgency of
preserving a neutral and innovative Internet. This essay is my attempt to share
some of that urgency with those who—through their voting and buying choices—will
ultimately decide the question of neutrality.

### [A Record Label from the Other Side](http://expos.mattyoder.com/crit1.html)
I first encountered Magnatune almost a year ago as I was searching for the
perfect Christmas gifts for some of my harder-to-buy-for family members. The two
albums that I purchased are still staples in my family's music collection, and
I've been a regular Magnatune customer ever since. In this review, I lend a
critical ear to the great music and "sound" business practices that make
Magnatune a hit.

### [Read Me (Only)](http://expos.mattyoder.com/cc1.html)
I have always be fascinated by the interactions between man and machine, creator
and created. As our lives become ever more intertwined with the latest gadgets
of our digital age, who is controlling whom becomes at once more interesting and
more difficult to determine. In this lighthearted essay, I probe multitasking,
one of the most pervasive marks of technology on our lives, and discover that
some activities are better left to our digital counterparts.

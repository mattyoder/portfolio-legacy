---
title: Preservation Alliance Interactive Map
date: 2013-08-01T12:00:00-06:00
draft: false
greeting: History.
project_url: http://www.preservephiladelphia.org/historic-resources/
description: >
  I built an interactive GIS map of historic properties for the Preservation
  Alliance for Greater Philadelphia.
---

# The Problem.
The Preservation Alliance for Greater Philadelphia had a wealth of information
about historic buildings and properties, but finding that information meant
digging through numerous maps and databases. The Alliance asked me to build a
single interactive map to bring together all of these historic resources using
open source tools.

## The Solution.
I began by consolidating and standardizing four existing inventories of historic
resources: African American Historic Sites, Historic Religious Sites,
Mid-Century Modern Architectural Resources and Pride of Place Neighborhood
Landmarks. After geocoding and de-duplicating these records, I built at PostGIS
spatial database that linked them to historic register designations and other
publicly-available data. Finally, I used Django to build an [interactive map
application](http://www.preservephiladelphia.org/historic-resources/) that makes
it easy to explore nearly 7,000 historic properties. In the process, I gained a
new appreciation for the history, architecture and cultural geography of
Philadelphia.

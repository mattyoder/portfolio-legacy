---
title: VitalSpark Visual Identity
date: 2008-11-01T12:00:00-06:00
draft: false
greeting: play god.
project_url: http://static.mattyoder.com/mattyoder/files/vitalspark.pdf
description: >
  In this graphic design project, I created a visual identity for VitalSpark, a
  fictitious genetic engineering startup. I designed a logo, stationary,
  advertisements, product package, and visual identity manual for a company that
  claimed to hold the key to life itself.
---

# The problem.
I'm the kind of artist who likes to play at the edge. So when I was asked to
design a visual identity for an imaginary company, the edge is where I went. In
bringing VitalSpark to life, I pushed the boundaries of what's possible and
what's ethical. I created an image for a genetic engineering firm that
encourages its customers to "play god," and in the process, I raised questions
about what it means to be a creator.

## The solution.
In designing VitalSpark, I wanted to create something that bridged the gap
between science fiction and everyday, something playful but with a dangerous
edge. I wanted the look to be cold but inviting, appealing but just a little bit
frightening.

For the logo, I chose the form of a double helix, the building block of life, as
the base form. I intersected one of its unraveling strands with a simple
starburst, evoking the "spark" in the company name. While the undulating lines
of the helix invite the viewer in, the sharp points of the starburst remind her
that this business of life is serious business. Playing god may be play, indeed,
but it's not without its risks.

---
title: Center for Mennonite Writing
date: 2009-01-01T12:00:00-06:00
draft: false
greeting: Menno.
project_url: http://www.mennonitewriting.org/
description: >
  I designed and coded a database-driven Web site for the new Center for
  Mennonite Writing. For the template, I abstracted images from the Fraktur art
  tradition in order to evoke the "Mennonite experience" without confining it.
---

# The problem.
The professors in the [Goshen College English
department](http://www.goshen.edu/english/Home) had a vision for an online
center dedicated to Mennonite writing. They wanted to create a space where
writers, readers and scholars could come together around this literary niche.
What they lacked were the tools to make their vision a reality.

## The process.
The [Center for Mennonite Writing](http://www.mennonitewriting.org/) grew out of
a [Maple Scholars](http://www.goshen.edu/academics/maple_scholars/) research
project during the summer of 2007. Professors Ann Hostetler and Kyle Schlabach
wrote a proposal to create an online space for the practice and discussion of
Mennonite writing. They selected me as their student researcher and asked me to
investigate the technologies for creating such a space and to create a prototype
of the site that could be used for further fund raising.

From the outset it was clear that the Center for Mennonite Writing would be
anchored by an online literary journal. Working with Kyle, I evaluated several
online journals and magazines to learn the best practices for creating this type
of publication. I also explored trends in Web 2.0 site-building to make the CMW
site interactive and user-driven.

Out of this research, I developed an organizational structure for the site that
included three sub-sites: [a journal](http://www.mennonitewriting.org/journal),
[an encyclopedia](http://www.mennonitewriting.org/encyclopedia) and [a community
section](http://www.mennonitewriting.org/community). These sub-sites would be
tied together under the Center for Mennonite Writing umbrella and brand, and
their content would be connected using a tagging system.


## The visual identity.
For the visual identity of the site, I was charged with creating a design that
captured "Mennonite experience," both past and present. For the logo, I selected
the image of the Anabaptist digger from the _Martyr's Mirror_ and simplified and
abstracted it. I then broke the CMW logo into is component shapes to form the
logos of each of the three sub-sites.

For the home page, I based the design on imagery from the bookplate of a 1708
Froschauer Bible. This image comes from the _fraktur_ tradition, an art form
closely tied to the historical Mennonite experience in some areas. As in the
logo, I abstracted the original imagery nearly beyond recognition, employing
suggestion rather than representation to create a connection to the Mennonite
tradition.

## The prototype.
To build the prototype for the site, I used the [Drupal](http://drupal.org/)
content management system, which was--and continues to be--one of the leading
CMSs for community-driven sites. By the end of the eight week research program,
I had constructed a functional Drupal site that included the three sub-sites and
a theme based on the visual identity I created, as well as custom views and
content types.


## The solution.
At the conclusion of the Maple Scholars term, the prototype was complete, but
the requirements of the project had evolved significantly. When Ann approached
me to build a final version of the site, I decided to re-implement it from the
ground up using [Django](http://djangoproject.com/), a Python Web framework.
This approach offered a number of advantages, including fine-grained control of
content types and presentation logic and an agile development style.

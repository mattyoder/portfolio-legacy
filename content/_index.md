---
title: Portfolio
date: 2018-12-25T12:00:00-06:00
draft: false
page_title: Welcome to the nest.
greeting: Hello.
description: >
  You’ve reached the home of _Matt Yoder._ Planner. Designer. Writer. Sometimes
  programmer. All-around bird-brain. Look around. Make yourself at home.
  _Enjoy!_
---

# Hi there.
My name is Matt. I work at the [Champaign County Regional Planning
Commission](https://ccrpc.org/), where I use civic technology to improve
transportation choices in my community.

The degree I hold is a [Master of Urban
Planning](http://urban.illinois.edu/academic-programs/mup/mup_overview.html),
but as I'm learning, planning isn't the kind of thing that can be mastered.
Economic factors, environmental concerns and social issues form a Gordian Knot
of competing interests that planners attempt to untangle. Some have called these
conundrums "[wicked problems](http://en.wikipedia.org/wiki/Wicked_problem)." For
me, they're wickedly fascinating.

Before I studied the seemingly intractable problems of cities, I worked on
equally stubborn challenges facing the environment. During three years at
[Groundwire](http://groundwire.org/), I built online tools for [some of the
smartest organizations in the sustainability
movement](http://groundwire.org/case_studies/). I learned the importance of data,
what good engagement looks like, and how people and systems combine to produce
change. (If this sounds like planning, it should.) In the process, I fell in
love with [Python](http://www.python.org/), a programming language that makes
coding an art.

Prior to my stint as a programmer for sustainability, I studied communication
and graphic design at [Goshen College](http://www.goshen.edu/), a small,
Mennonite, liberal-arts college in northern Indiana. Goshen taught me to write,
speak and design, but it also exploded my worldview. The seeds of my planning
education were planted in a [food pantry in
Denver](http://www.goshen.edu/cmin/inquiry_programs/service_inquiry/) and in the
[Andes of Peru](https://www.goshen.edu/peru/). Transformative hardly begins to
describe it.

Clearly I like to plan. I also like to play Scrabble, rollerblade, strum the
ukulele and print things the old-fashioned way. Want to know more? [Just
ask.](contact)

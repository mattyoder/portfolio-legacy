---
title: Discovering Downtown Urbana
date: 2013-04-19T12:00:00-06:00
draft: false
greeting: Design.
project_url: http://static.mattyoder.com/mattyoder/files/downtown-urbana.pdf
description: >
  In this urban design class project, I analyzed the elements that make up
  central Urbana to define a downtown neighborhood.
---

# The problem.
Definitions are important in planning, but placing a label on the urban
experience can be challenging. Urban planner Kevin Lynch attempted to describe
the "image of the city" using five elements: nodes, landmarks, paths, edges and
districts. In this urban design class project, I set out to identify a downtown
neighborhood for the city of Urbana using Lynch's five elements.

## The solution.
My analysis of urban form in central Urbana yielded three possible downtown
neighborhoods. The Commercial Core definition set the boundaries based on
existing uses and barriers, while Urbana Crossings attempted to increase
connectivity using seams and paths. Main Street Plus, the preferred definition,
brought together activity and connectivity by building on the elements that make
Downtown Urbana unique: historic Main Street and Lincoln Square Village.

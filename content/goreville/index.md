---
title: Goreville Village Vision
date: 2014-05-15T12:00:00-06:00
draft: false
greeting: Plan.
project_url: http://gorevillevision.com/
description: >
  Chris Francis and I created an online comprehensive plan for the Village of
  Goreville, Illinois.
---

# The Problem.
Comprehensive planning is usually a slow and expensive process. That's part of
the reason that the Village of Goreville, located in the Shawnee Hills region of
southern Illinois, hadn't updated its plan in fifty years. By combining online
tools with traditional public engagement strategies, my colleague Chris Francis
and I were able to create a new comprehensive plan for the Village in less than
a year.

## The Solution.
[Village Vision](http://gorevillevision.com/) is a plan, but it's also a
website. Instead of sitting on the shelf like many comprehensive plans, the
Village Vision site allows community leaders and members of the public to access
maps, data and recommendations from their computers or smartphones. In graphics
and text, it brings together a wealth of resources including:

* Community survey results
* Population and employment projections
* Land use maps
* Traffic counts
* Bicycle and pedestrian level of service
* Housing statistics
* Shopping behavior
* Economic development resources

More importantly, the site links these data sources directly to plan elements
and recommendations, allowing visitors to see how the vision is grounded in
community priorities and research. To learn more about the Village Vision plan
and process, check out the [project
poster](http://gorevillevision.com/wp-content/uploads/2014/05/villagevision_poster.pdf),
download a two-page [summary
document](http://gorevillevision.com/wp-content/uploads/2014/03/villagevision_summary.pdf)
or explore the [online plan](http://gorevillevision.com/).

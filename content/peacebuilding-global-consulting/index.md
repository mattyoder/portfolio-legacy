---
title: Peacebuilding Global Consulting
date: 2009-04-01T12:00:00-06:00
draft: false
greeting: Peace.
project_url: http://www.peacebuildingglobal.com/
description: >
  Jon Rudy approached me to create a simple yet elegant site for his
  peacebuidling consulting practice. This site, with its clean design and
  international flair, is the result.
---

# The problem.
Jon Rudy approached me to create an online presence site for his peacebuidling
consulting practice. He wanted a site that would highlight his areas of
expertise and demonstrate his experience transforming conflicts in international
settings. He also needed a site that was simple to maintain and integrated with
his existing tools--all this on a limited budget.

## The solution.
The first step in my work with Jon was identifying content to feature on the
site. To begin, we reviewed the content of his previous site as well as the
materials that he used to communicate with clients. Jon had already identified
six areas of expertise--from peace program design to nonviolence training.  I
paired these with images from his photo library to create an engaging display
for the site's home page.

On the technical front, I evaluated several content management systems, but all
of them seemed like overkill for this simple site. Instead, I created a simple
custom CMS using PHP and integrated it with [CushyCMS](http://www.cushycms.com/)
for easy editing.  I also integrated Jon's Google Calendar and pulled in posts
from [his blog](http://jonsjourneyswithpeace.blogspot.com/) so that he wouldn't
have to edit content in multiple locations.

For design on a tight budget, I looked to [Adobe's Kuler
tool](http://kuler.adobe.com/) for inspiration. I selected the [Sushi Maki
palette by lilipruneau](http://kuler.adobe.com/#themeID/350205) to reflect Jon's
lively personality and international savvy. The design that I created from it
combines clean lines with bold colors to create a look that's traditional with a
little extra zing.

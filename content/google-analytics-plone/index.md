---
title: Google Analytics for Plone
date: 2010-05-01T12:00:00-06:00
draft: false
greeting: Stats.
project_url: http://plone.org/products/collective.googleanalytics
description: >
  During my time at Groundwire, I developed an integration between Google
  Analytics and Plone, allowing our environmental groups to keep closer tabs on
  their statistics.
---

# The problem.
Google Analytics is great. It provides objective data about Web site traffic
that can help site editors make important decisions about their content. The
trouble is, Analytics data lives at Google, and most content editors spend the
majority of time on their own sites.

## The solution.

[Groundwire](http://groundwire.org) asked me to create a tool to bring Analytics
data back into Plone, where content editors spend most of their time. [Google
Analytics for Plone](http://plone.org/products/collective.googleanalytics) (or
collective.googleanalytics in Plone-speak) does that and more. It provides a
full suite of tools for tracking additional data in Google Analytics and for
reporting on statistics within the Plone site.

Want to learn more? Watch the talk I gave at [Groundwire's speed-geek
event](http://groundwire.org/blog/speedy-and-geeky) in March 2010:

<object id="scPlayer" class="embeddedObject" width="460" height="345" type="application/x-shockwave-flash" data="http://content.screencast.com/users/yomatters/folders/Jing/media/7fa92b90-133d-460d-b702-64d69883fc43/jingswfplayer.swf" >  <param name="movie" value="http://content.screencast.com/users/yomatters/folders/Jing/media/7fa92b90-133d-460d-b702-64d69883fc43/jingswfplayer.swf" />  <param name="quality" value="high" />  <param name="bgcolor" value="#FFFFFF" />  <param name="flashVars" value="thumb=http://content.screencast.com/users/yomatters/folders/Jing/media/7fa92b90-133d-460d-b702-64d69883fc43/FirstFrame.jpg&containerwidth=460&containerheight=345&content=http://content.screencast.com/users/yomatters/folders/Jing/media/7fa92b90-133d-460d-b702-64d69883fc43/00000003.swf&blurover=false" />  <param name="allowFullScreen" value="true" />  <param name="scale" value="showall" />  <param name="allowScriptAccess" value="always" />  <param name="base" value="http://content.screencast.com/users/yomatters/folders/Jing/media/7fa92b90-133d-460d-b702-64d69883fc43/" /> </object>

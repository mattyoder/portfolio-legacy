---
title: Cost-Benefit Analysis of the Schuylkill River Trail
date: 2012-09-15T12:00:00-06:00
draft: false
greeting: Trails.
project_url: http://static.mattyoder.com/mattyoder/files/schuylkill-river-trail.pdf
description: >
  I analyzed the relative costs and benefits of an urban trail in Philadelphia.
---

# The Problem.
Urban trails are frequently described in terms of their health benefits to a
community, but they also offer other types of benefits: recreation, mobility,
and reductions in automobile use, among others. Unfortunately for planners and
advocates of active transportation, these benefits can be difficult to quantify.

## The Solution.
Using techniques derived from recent transportation planning research, I
estimated the benefits and costs of the Schuylkill River Trail, a urban trail
that passes through northern and central Philadelphia. The benefits outweighed
the costs in eight out of nine cost-benefit scenarios, with a benefit-cost ratio
of 2.77 in the most likely scenario. Under this scenario, recreation and health
accounted for about 95 percent of the total benefits. I also found that, though
small, the mobility benefit was increasing in importance based on a spatial
analysis of bicycle commuting trends.

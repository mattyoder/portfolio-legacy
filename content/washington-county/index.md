---
title: Regional Analysis of Washington County
date: 2013-03-07T12:00:00-06:00
draft: false
greeting: Data.
project_url: http://static.mattyoder.com/mattyoder/files/washington-county.pdf
description: >
  In this data analysis project, I explored the relationship between the
  physical crossroads that defined Washington County’s past and the metaphorical
  crossroads that promises to shape its future.
---

# The problem.
The Census Bureau and other federal agencies collect a wealth of information
about communities across the United States. Unfortunately, much of this
information—from population, to housing to labor statistics—is buried in
complicated tables that are difficult to interpret. Making sense of all this
information and using it to tell a story that can help to shape decisions is the
work of the data analyst, and it is the task I took on in this class project.

## The solution.
Washington County, Maryland is a county at a crossroads, both literally and
metaphorically. Located at the intersection of I-70 and I-81, the county's
economy has historically depended on manufacturing and transportation. In recent
decades, however, expansion of the Washington, D.C. metropolitan area has placed
Washington County within the reach of suburban commuters. Using data as its
foundation, this report explores the relationship between the physical
crossroads that defined Washington County’s past and the metaphorical
crossroads—defined by commuting and migration trends—that promises to shape its
future.

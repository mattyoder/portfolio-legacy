---
title: Rooftop Garden GIS Analysis
date: 2012-09-01T12:00:00-06:00
draft: false
greeting: Green.
project_url: http://static.mattyoder.com/mattyoder/files/rooftop-gardens.pdf
description: >
  Using ArcGIS and data from the city of Chicago, I identified candidate
  locations for rooftop gardens in five neighborhoods on Chicago's West Side.
---

# The problem.
With vegetation atop more than 300 buildings, Chicago has been recognized as a
leader in green roofs. These vegetated rooftops help to reduce runoff and
mitigate the urban heat island effect.

Though the city has been recognized for its efforts toward sustainability,
Chicago also has been the target of recent criticism related to its food
systems. In particular work by Michelle Obama and others has drawn attention to
so-called "food deserts"—areas without access to fresh produce and other healthy
foods.

## The solution.
Though still relatively uncommon, rooftop gardens promise to provide the
benefits green roofs while also addressing shortages of fresh produce among
urban dwellers. In this class project, I evaluated candidate buildings for
rooftop gardens in five neighborhoods on Chicago’s West Side: Austin, Humboldt
Park, West Garfield Park, East Garfield Park and North Lawndale. These
neighborhoods are home to just eighteen buildings with green roofs.

I evaluated six criteria, including the locations of existing green roofs,
community gardens, farmers markets and schools; the roof area of the building;
and the potential for heat island reduction as determined by the color of the
roof. Based on these criteria, I assigned each building a suitability score and
identified the three most suitable buildings in each neighborhood.

---
title: Suitability Analysis for Champaign County
date: 2012-10-21T12:00:00-06:00
draft: false
greeting: Layers.
project_url: http://static.mattyoder.com/mattyoder/files/suitability-analysis.pdf
description: >
  Using a technique developed by landscape architect Ian McHarg, I identified
  the most suitable areas for development in Champaign County, Illinois.
---

# The problem.
Growth in Champaign County is taking place at the edges of three
cities—Champaign, Urbana and Mahomet—but all of these growth areas are not
created equal. Environmental factors make some areas more suitable for
development and others more valuable for preservation. In this class project, I
set out to find the most suitable locations for urban growth.

## The solution.
In his 1969 book _Design with Nature_, Landscape architect Ian McHarg pioneered
a method for considering multiple natural systems in a single suitability
analysis. McHarg's method overlays transparent layers: one for each natural
system under consideration. In the resulting map, the lightest areas are the
most suitable for development.

In this report, I apply McHarg's method to Champaign County, Illinois, the home
of the University of Illinois. Using layers representing slope, soil, surface
water and green infrastructure, my composite map reveals that the growth area
east of Mahomet is the most suitable for development.

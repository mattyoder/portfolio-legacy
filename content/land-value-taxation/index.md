---
title: Land Value Taxation in Philadelphia
date: 2012-12-16T12:00:00-06:00
draft: false
greeting: Equity.
project_url: http://static.mattyoder.com/mattyoder/files/lvt-philadelphia.pdf
description: >
  In this research paper, I examined the impacts of land value taxation on
  vertical equity in Philadelphia.
---

# The Problem.
Despite serving as a major source of revenue, the real property tax has been
troublesome for Philadelphia. Until the recent city-wide reassessment, tax
assessments were highly inconsistent across the city. In 2012, there were more
than 100,000 delinquent properties, and owners of these properties owed the city
and school district $515.4 million in back taxes and interest.

## The Solution.
First proposed by Henry George, a native of Philadelphia, land value taxation
(LVT) is an alternative to the traditional real property tax in which only land
value is taxed. By excluding buildings and improvements from the assessment, LVT
is theorized to reduce land speculation, encourage maintenance of properties and
limit the shifting of taxes from owners to renters. Despite these advantages,
LVT has been implemented in relatively few U.S. cities.

Using a PostGIS database and tax records published by the Philadelphia Office of
Property Assessment, I examined the impact of LVT on vertical equity in
Philadelphia's property tax system. I found that implementing LVT would make the
property tax more progressive than either the 2013 or 2014 tax systems. In
addition, it would shift the tax burden from residential properties to vacant
land while imposing a similar burden to the 2013 tax system on commercial and
industrial properties.
